self = (if window? then window.PokeBattle.conditions ?= {} else this)

@Conditions =
  TEAM_PREVIEW             : 1
  RATED_BATTLE             : 2
  PBV_1000                 : 3
  TIMED_BATTLE             : 4
  SLEEP_CLAUSE             : 5
  SPECIES_CLAUSE           : 6
  EVASION_CLAUSE           : 7
  OHKO_CLAUSE              : 8
  UNRELEASED_BAN           : 9
  PRANKSTER_SWAGGER_CLAUSE : 10
  PBV_500                  : 11
  VISIBLE_TEAM             : 12
  PRIMAL_LIMIT             : 13
  TIER_LC                  : 14
  TIER_PU                  : 15
  TIER_NU                  : 16
  TIER_RU                  : 17
  TIER_UU                  : 18
  TIER_OU                  : 19
  TIER_Uber                : 20

@SelectableConditions = [
  @Conditions.VISIBLE_TEAM
  @Conditions.TIMED_BATTLE
  @Conditions.SLEEP_CLAUSE
  @Conditions.EVASION_CLAUSE
  @Conditions.SPECIES_CLAUSE
  @Conditions.PRANKSTER_SWAGGER_CLAUSE
  @Conditions.OHKO_CLAUSE
  @Conditions.UNRELEASED_BAN
]

@HumanizedConditions =
  en:
    VISIBLE_TEAM   : "Visible Teams"
    TEAM_PREVIEW   : "Team Preview"
    SLEEP_CLAUSE   : "Sleep Clause"
    RATED_BATTLE   : "Rated Battle"
    TIMED_BATTLE   : "Timed Battle"
    SPECIES_CLAUSE : "Species Clause"
    EVASION_CLAUSE : "Evasion Clause"
    OHKO_CLAUSE    : "One-Hit KO Clause"
    UNRELEASED_BAN : "Unreleased Ban"
    PRANKSTER_SWAGGER_CLAUSE : "Prankster + Swagger Clause"
    PRIMAL_LIMIT   : "Primal Limit"
    

self.PresetFormats =
  insur1000:
    name       : 'insur1000'
    humanName  : 'Insurgence 1000'
    generation : 'in'
    conditions : [ @Conditions.PBV_1000, @Conditions.TEAM_PREVIEW, @Conditions.PRIMAL_LIMIT]
    playable   : true
    priority   : 5
    tierBased  : false
  xy1000:
    name       : 'xy1000'
    humanName  : '1,000 PBV XY'
    generation : 'xy'
    conditions : [ @Conditions.PBV_1000, @Conditions.TEAM_PREVIEW, @Conditions.PRIMAL_LIMIT]
    playable   : true
    priority   : 10
    tierBased  : false
  xy500:
    name       : 'xy500'
    humanName  : '500 PBV XY'
    generation : 'xy'
    conditions : [ @Conditions.PBV_500, @Conditions.TEAM_PREVIEW, @Conditions.PRIMAL_LIMIT]
    playable   : true
    priority   : 10
    tierBased  : false

@GenerationCondition = 
  'in':
    conditionname  : 'insur'
    humanName      : 'Insurgence'

self.Formats_ = {}

self.Formats = ->
  if Object.keys(@Formats_).length is 0
    @setFormats()
    return @Formats_
  else
    return @Formats_

self.setFormats = ->
  playablegens = ['in']
  newformats = @PresetFormats
  if window?
    Tiers = window.PokeBattle.Tier.Tiers
    conditions = Conditions
    _ = window._
  else
    {Tiers} = require('./tier')
    conditions = @Conditions
    {_} = require('underscore')
  for key of conditions
    if (key.search /TIER_/) == 0
      tier = key.replace /TIER_/, ""
      tierData = Tiers[tier]
      if tierData.playable
        for gen in playablegens
          tierbased = true
          if window?
            formatname = GenerationCondition[gen].conditionname + tier
            _conditions = [Conditions[key], Conditions.TEAM_PREVIEW, Conditions.PRIMAL_LIMIT]
            priority = tierData.priority
            humanname = "#{GenerationCondition[gen].humanName} #{tierData.humanName}"
          else
            formatname = @GenerationCondition[gen].conditionname + tier
            _conditions = [@Conditions[key], @Conditions.TEAM_PREVIEW, @Conditions.PRIMAL_LIMIT]
            priority = tierData.priority
            humanname = "#{@GenerationCondition[gen].humanName} #{tierData.humanName}"
          
          newformats[formatname] = {name: formatname, humanName: humanname, generation: gen, conditions: _conditions, priority: priority, tierBased: tierbased}
  sortedformatsarr = _.sortBy(newformats, 'priority');
  finalobj = {}
  for format in sortedformatsarr
    finalobj[format.name] = format
  @Formats_ = finalobj

@DEFAULT_FORMAT = 'insurOU'

